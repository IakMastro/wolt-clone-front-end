export interface Restaurant {
  id: number
  name: string
  logo: string
  type: string
  price: string
}