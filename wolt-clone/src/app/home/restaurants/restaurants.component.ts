// FIXME: Fix bug in which the table is not sorted correctly.
import { DecimalPipe } from '@angular/common';
import { Component, OnInit, QueryList, ViewChildren } from '@angular/core';
import { Observable } from 'rxjs/internal/Observable';
import { SortableDirective, SortEvent } from 'src/app/sortable.directive';
import { Restaurant } from 'src/lib/restaurant';
import { RestaurantService } from 'src/restaurant.service';

@Component({
  selector: 'app-restaurants',
  templateUrl: './restaurants.component.html',
  styleUrls: ['./restaurants.component.scss'],
  providers: [RestaurantService, DecimalPipe]
})
export class RestaurantsComponent {
restaurants$: Observable<Restaurant[]>
  total$: Observable<number>

  @ViewChildren(SortableDirective)
  headers!: QueryList<SortableDirective>;

  constructor(public service: RestaurantService) {
    this.restaurants$ = service.restaurants$
    this.total$ = service.total$
  }

  onSort({column, direction}: SortEvent) {
    this.headers.forEach(header => {
      if (header.sortable !== column) {
        header.direction = ''
      }
    })

    this.service.sortColumn = column
    this.service.sortDirection = direction
  }
}
