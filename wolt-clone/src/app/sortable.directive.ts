import { Directive, Input, Output, EventEmitter } from '@angular/core';
import { Restaurant } from 'src/lib/restaurant';

export type SortColumn = keyof Restaurant | ''
export type SortDirection = 'asc' | 'desc' | ''
const rotate: {[key: string]: SortDirection} = { 'asc': 'desc', 'desc': '', '': 'asc' }

export interface SortEvent {
  column: SortColumn
  direction: SortDirection
}
@Directive({
  selector: 'th[appSortable]',
  host: {
    '[class.asc]': 'direction === "asc"',
    '[class.desc]': 'direction === "desc"',
    '(click)': 'rotate()'
  }
})
export class SortableDirective {
  @Input() sortable: SortColumn = ''
  @Input() direction: SortDirection = ''
  @Output() sort = new EventEmitter<SortEvent>()

  rotate() {
    this.direction = rotate[this.direction]
    this.sort.emit({column: this.sortable, direction: this.direction})
  }
}
